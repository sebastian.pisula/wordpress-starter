export project=${PWD##*/}

wp core download --force
wp config create --dbname=${project} --dbprefix=${project}_ --force
wp db create --quiet
wp core install --url=https://localhost/proadax.pl/${project} --title=${project} --admin_user=sebastian.pisula --admin_email=sebastian.pisula@gmail.com
wp rewrite structure '/%postname%/'
wp option update blogdescription ''
wp option update timezone_string 'Europe/Warsaw'
wp plugin install filenames-to-latin --activate --force
wp plugin install query-monitor --activate --force
wp plugin install wordpress-seo --activate --force
wp plugin install "http://connect.advancedcustomfields.com/index.php?p=pro&a=download&k=b3JkZXJfaWQ9MTc2ODE4fHR5cGU9ZGV2ZWxvcGVyfGRhdGU9MjAxOS0xMS0yNCAwOTowMToxMw==" --activate --force
wp plugin delete hello
wp plugin delete akismet
wp plugin activate website-functionality
wp theme activate custom
wp theme delete twentynineteen
wp theme delete twentytwenty
wp theme delete twentytwentyone
wp post delete 1 --force
wp post update 2 --post_name=home --post_title="Home" --post_content=''
wp option update show_on_front page
wp option update page_on_front 2
wp widget reset --all
wp user meta add 1 wporg_favorites sebastianpisula
wp user update 1 --display_name="Sebastian Pisula"
wp rewrite flush

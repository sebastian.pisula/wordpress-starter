<?php

add_action( 'wpsdb_migration_complete', function ( $action ) {
	if ( 'pull' === $action ) {
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

		activate_plugin( 'user-switching/user-switching.php' );
		activate_plugin( 'query-monitor/query-monitor.php' );
		activate_plugin( 'simply-show-hooks/index.php' );
	}
} );
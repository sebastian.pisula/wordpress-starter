<?php

class IC_Active_Menu_Item_Scroll {

	public function add_hooks() {
		add_filter( 'nav_menu_link_attributes', [ $this, 'nav_menu_link_attributes' ], 10, 4 );
	}

	/**
	 * @param array   $atts
	 * @param WP_Post $item
	 * @param array   $args
	 * @param int     $depth
	 *
	 * @return array
	 */
	public function nav_menu_link_attributes( $atts, $item, $args, $depth ) {
		if ( 'custom' === $item->type && substr( $item->url, 0, 1 ) === '#' ) {
			$atts['href'] = trailingslashit( home_url() ) . $item->url;
		}

		return $atts;
	}

	/**
	 * @param WP_Post $item .
	 *
	 * @return bool
	 */
	private function is_active_item( $item ) {
		return 'type' === $item->type && is_singular( $item->object );
	}
}

( new IC_Active_Menu_Item_Scroll )->add_hooks();

<?php

class IC_Cache_Enabler {

	public function add_hooks() {
		add_filter( 'mod_rewrite_rules', [ $this, 'mod_rewrite_rules' ] );
	}

	public function mod_rewrite_rules( $rules ) {
		if ( ! ic_is_production() ) {
			return $rules;
		}

		if ( ! defined( 'CACHE_ENABLER_VERSION' ) ) {
			return $rules;
		}

		$rules = "# BEGIN Cache Enabler

<IfModule mod_rewrite.c>
    <IfModule mod_setenvif.c>
        RewriteEngine On
        RewriteBase /

        # cache directory
        SetEnvIf Host ^ CE_CACHE_DIR=/wp-content/cache/cache-enabler

        # default cache keys
        SetEnvIf Host ^ CE_CACHE_KEY_SCHEME http-
        SetEnvIf Host ^ CE_CACHE_KEY_DEVICE
        SetEnvIf Host ^ CE_CACHE_KEY_WEBP
        SetEnvIf Host ^ CE_CACHE_KEY_COMPRESSION

        # scheme cache key
        RewriteCond %{HTTPS} ^(on|1)$ [OR]
        RewriteCond %{SERVER_PORT} =443 [OR]
        RewriteCond %{HTTP:X-Forwarded-Proto} =https [OR]
        RewriteCond %{HTTP:X-Forwarded-Scheme} =https
        RewriteRule ^ - [E=CE_CACHE_KEY_SCHEME:https-]

        # device cache key
        # SetEnvIf User-Agent \"(Mobile|Android|Silk/|Kindle|BlackBerry|Opera Mini|Opera Mobi)\" CE_CACHE_KEY_DEVICE=-mobile

        # webp cache key
        # SetEnvIf Accept image/webp CE_CACHE_KEY_WEBP=-webp

        # compression cache key
        # <IfModule mod_mime.c>
        #     SetEnvIf Accept-Encoding gzip CE_CACHE_KEY_COMPRESSION=.gz
        #     AddType text/html .gz
        #     AddEncoding gzip .gz
        # </IfModule>

        # get cache file
        SetEnvIf Host ^ CE_CACHE_FILE_DIR=%{ENV:CE_CACHE_DIR}/%{HTTP_HOST}%{REQUEST_URI}
        SetEnvIf Host ^ CE_CACHE_FILE_NAME=%{ENV:CE_CACHE_KEY_SCHEME}index%{ENV:CE_CACHE_KEY_DEVICE}%{ENV:CE_CACHE_KEY_WEBP}.html%{ENV:CE_CACHE_KEY_COMPRESSION}
        SetEnvIf Host ^ CE_CACHE_FILE=%{ENV:CE_CACHE_FILE_DIR}/%{ENV:CE_CACHE_FILE_NAME}

        # check if cache file exists
        RewriteCond %{DOCUMENT_ROOT}%{ENV:CE_CACHE_FILE} -f

        # check request method
        RewriteCond %{REQUEST_METHOD} =GET

        # check permalink structure has trailing slash
        RewriteCond %{REQUEST_URI} /[^\./\?]+(\?.*)?$
        # check permalink structure has no trailing slash
        # RewriteCond %{REQUEST_URI} /[^\./\?]+/(\?.*)?$

        # check excluded query strings
        RewriteCond %{QUERY_STRING} !^(?!(fbclid|ref|mc_(cid|eid)|utm_(source|medium|campaign|term|content|expid)|gclid|fb_(action_ids|action_types|source)|age-verified|usqp|cn-reloaded|_ga|_ke)).+$

        # check excluded cookies
        RewriteCond %{HTTP_COOKIE} !(wp-postpass|wordpress_logged_in|comment_author)_

        # deliver cache file
        RewriteRule ^ %{ENV:CE_CACHE_FILE} [L]
    </IfModule>
</IfModule>

# END Cache Enabler\n\n" . $rules;

		return $rules;
	}
}

( new IC_Cache_Enabler() )->add_hooks();

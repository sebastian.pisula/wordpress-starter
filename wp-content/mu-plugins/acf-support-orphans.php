<?php

class IC_ACF_Support_Orphans {
	/**
	 * Register hooks.
	 */
	public function add_hooks() {
		add_filter( 'acf/format_value/type=textarea', [ $this, 'fix_orphans' ] );
		add_filter( 'acf/format_value/type=wysiwyg', [ $this, 'fix_orphans' ] );
		add_filter( 'acf/format_value/type=text', [ $this, 'fix_orphans' ] );
	}

	public function fix_orphans( $value ) {
		if ( class_exists( 'iworks_orphan' ) ) {
			$orphan = new iworks_orphan();

			return $orphan->replace( $value );
		}

		return $value;
	}
}

( new IC_ACF_Support_Orphans() )->add_hooks();

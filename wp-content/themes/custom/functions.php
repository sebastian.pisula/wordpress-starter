<?php
/**
 * Functions file.
 *
 * @package Theme
 */

use Theme\Core\Base;

include get_parent_theme_file_path( 'vendor/autoload.php' );

new class() extends Base {
	/**
	 *  constructor.
	 */
	public function __construct() {
		add_filter( 'theme/flexible_content/exclude_layouts', [ $this, 'exclude_layouts' ], 10, 3 );

		parent::__construct();
	}

	/**
	 * @param array          $exclude_fields
	 * @param array          $layouts
	 * @param WP_Screen|null $screen
	 *
	 * @return array
	 */
	public function exclude_layouts( $exclude_fields, $layouts, $screen ) {
		return $exclude_fields;
	}

	/**
	 * Google API Key
	 *
	 * @var string
	 */
	protected $google_api_key = 'AIzaSyBHl0Q9cxieJYCYFqG-Hozivf_xz6WGnCM';

	/**
	 * Register custom menu
	 */
	public function register_menus() {
		register_nav_menus( [ 'primary' => __( 'Primary Menu', 'theme' ) ] );
	}

	/**
	 * Before main script and style.
	 *
	 * @param array $urls Array of theme urls.
	 * @param array $dirs Array of theme dirs.
	 */
	public function register_scripts_before( $urls, $dirs ) {
		// Google Maps.
		$url = add_query_arg( 'key', $this->google_api_key, '//maps.googleapis.com/maps/api/js' );
		wp_register_script( 'google-maps', $url, [], '3', true );
	}

	/**
	 * Before main script and style.
	 *
	 * @param array $urls Array of theme urls.
	 * @param array $dirs Array of theme dirs.
	 */
	public function register_scripts_after( $urls, $dirs ) {
	}

	/**
	 * Deps for css.
	 *
	 * @param array $deps An array of registered script handles this script depends on. Default empty array.
	 *
	 * @return array
	 */
	public function style_deps( $deps ) {
		return $deps;
	}

	/**
	 * Deps for JS.
	 *
	 * @param array $deps An array of registered script handles this script depends on. Default empty array.
	 *
	 * @return array
	 */
	public function script_deps( $deps ) {
		return $deps;
	}

	/**
	 * Deps for CS and JS.
	 *
	 * @param array $deps An array of registered script handles this script depends on. Default empty array.
	 *
	 * @return array
	 */
	public function both_deps( $deps ) {
		return $deps;
	}

	/**
	 * Main script localize.
	 *
	 * @param array $vars Array vars for JS.
	 *
	 * @return array
	 */
	public function localize_script( $vars ) {
		return $vars;
	}

	/**
	 * Register image sizes.
	 */
	public function add_image_sizes() {
	}

	/**
	 * Register sidebar.
	 */
	public function register_sidebars() {
		register_sidebar(
			[
				'name'          => 'Bottom sidebar',
				'id'            => 'bottom',
				'description'   => '',
				'before_widget' => '<div id="%1$s" class="widget col %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			]
		);
	}

	/**
	 * Register Widgets.
	 */
	public function register_widgets() {
	}
};

<?php
/**
 * Disable Embed
 *
 * @package Theme
 */

namespace Theme\Module;

/**
 * Class Disable_Embed
 * Source: http://crunchify.com/how-to-disable-auto-embed-script-for-wordpress-4-4-wp-embed-min-js/
 */
class Disable_Embed {
	/**
	 * Disable_Embed constructor.
	 */
	public function __construct() {
		// Remove the REST API endpoint.
		remove_action( 'rest_api_init', 'wp_oembed_register_route' );

		// Turn off oEmbed auto discovery.
		add_filter( 'embed_oembed_discover', '__return_false' );

		// Don't filter oEmbed results.
		remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

		// Remove oEmbed discovery links.
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

		// Remove oEmbed-specific JavaScript from the front-end and back-end.
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );

		add_action( 'template_redirect', [ $this, 'template_redirect' ] );
	}

	/**
	 * Fires before determining which template to load.
	 */
	public function template_redirect() {
		wp_deregister_script( 'wp-embed' );
	}
}

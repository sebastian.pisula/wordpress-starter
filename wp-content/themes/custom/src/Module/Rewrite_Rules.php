<?php
/**
 * Rewrite rules for dynamic file names
 *
 * @package Theme
 */

namespace Theme\Module;

use WP_Rewrite;
use WP_Theme;

/**
 * Class Rewrite_Rules
 *
 * @package Theme\Module
 */
class Rewrite_Rules {
	/**
	 * Rewrite_Rules constructor.
	 */
	public function __construct() {
		add_action( 'after_switch_theme', [ $this, 'after_switch_theme' ], 10, 2 );
		add_action( 'generate_rewrite_rules', [ $this, 'generate_rewrite_rules' ] );
	}

	/**
	 * Fires after the rewrite rules are generated.
	 *
	 * @param WP_Rewrite $wp_rewrite Current WP_Rewrite instance (passed by reference).
	 */
	public function generate_rewrite_rules( $wp_rewrite ) {
		$template = wp_normalize_path( get_template_directory() . '/' );
		$path     = str_replace( wp_normalize_path( ABSPATH ), '', $template );
		$path     = str_replace( '\\', '/', $path );

		$wp_rewrite->add_external_rule(
			$path . 'assets/(.*)(-time-[0-9]+)(\..*)',
			$path . 'assets/$1$3'
		);
	}

	/**
	 * Fires on the first WP load after a theme switch if the old theme still exists.
	 * This action fires multiple times and the parameters differs
	 * according to the context, if the old theme exists or not.
	 * If the old theme is missing, the parameter will be the slug
	 * of the old theme.
	 *
	 * @param string   $old_name  Old theme name.
	 * @param WP_Theme $old_theme WP_Theme instance of the old theme.
	 */
	public function after_switch_theme( $old_name, $old_theme ) {
		add_action( 'admin_init', 'save_mod_rewrite_rules', 1000 );
	}
}

<?php
/**
 * Theme template.
 *
 * @package Theme
 */

?>
<footer class="footer-main">
	<div class="container">
		<div class="row">
			<?php dynamic_sidebar( 'bottom' ); ?>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>

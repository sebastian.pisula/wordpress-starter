<?php
/**
 * Theme template.
 *
 * @package Theme
 */

get_header(); ?>

	<div>
		<?php esc_html_e( '404 Not Found', 'theme' ); ?>
	</div>

<?php
get_footer();

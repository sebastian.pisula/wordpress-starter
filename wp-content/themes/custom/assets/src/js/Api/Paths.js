export const baseApiUrl = `${__jsVars.url.api}v1/`;

export const paths = {
    example: 'example/',
    exampleById: 'example/{id}',
};
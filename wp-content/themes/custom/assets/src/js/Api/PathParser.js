export function pathParser(path = '', params = {}) {
    for (const param in params){
        path = path.replace(new RegExp(`{${param}}`, 'g'), params[param]);
    }

    return path;
}
/**
 * AXIOS
 * @github: https://github.com/axios/axios
 */
import axios from 'axios';
import { paths, baseApiUrl } from './Paths';
import ENV from '../Config/Env';
import { pathParser } from './PathParser';

const instance = axios.create({
    baseURL: baseApiUrl,
    timeout: ENV.requestTimeout,
    headers: {}, // Global headers for all request add here
});

export const getExample = () => axios.get(paths.example);

export const getExampleById = (id = '') => axios.get(pathParser(paths.exampleById, { id }));
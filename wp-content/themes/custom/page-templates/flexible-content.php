<?php
/**
 * Template Name: Flexible Content
 */

global $section_id;

the_post();
get_header(); ?>

	<section class="page-content">
		<?php ic_flexible_content(); ?>
	</section>

<?php get_footer();
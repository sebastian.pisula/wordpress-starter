<?php
/**
 * Theme template.
 *
 * @package Theme
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<div>
		<h3><?php the_title(); ?></h3>

		<div><?php the_excerpt(); ?></div>
		<div><a href="<?php the_permalink(); ?>">Zobacz</a></div>
	</div>
<?php endwhile; ?>

<?php echo paginate_links(); ?>

<?php
get_footer();

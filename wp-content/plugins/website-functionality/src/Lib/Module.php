<?php

namespace Website_Functionality\Lib;

use Website_Functionality\Plugin;

/**
 * Class Module
 *
 * @package Website_Functionality\Lib
 */
abstract class Module {
	/** @var Plugin */
	protected $plugin;

	/**
	 * Module constructor.
	 *
	 * @param Plugin $plugin
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;
	}

	/**
	 *
	 */
	public function add_hooks() {

	}
}
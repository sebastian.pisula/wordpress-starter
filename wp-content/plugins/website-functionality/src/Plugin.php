<?php

namespace Website_Functionality;

use Website_Functionality\Module\Example;

/**
 * Class Plugin
 *
 * @package Website_Functionality
 */
class Plugin {
	/**
	 * @var string
	 */
	private $file;

	/**
	 * Plugin constructor.
	 *
	 * @param string $file
	 */
	public function __construct( string $file ) {
		$this->file = $file;
	}

	/**
	 * Register plugin modules
	 */
	public function add_hooks() {
		( new Example( $this ) )->add_hooks();
	}

	/**
	 * @param string $path
	 *
	 * @return string
	 */
	public function get_plugin_path( string $path = '' ): string {
		return wp_normalize_path( plugin_dir_path( $this->file ) . '/' . $path );
	}

	/**
	 * @param string $file
	 *
	 * @return string
	 */
	public function get_plugin_url( string $file = '' ): string {
		return plugins_url( $file, $this->file );
	}

	/**
	 * @return string
	 */
	public function get_plugin_basename(): string {
		return plugin_basename( $this->file );
	}
}